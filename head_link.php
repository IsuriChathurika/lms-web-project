<!DOCTYPE html>
<html lang="en">
  <head>
        <title>Blackboard LMS</title>

        <meta charset="UTF-8">
        <meta name="description" content="Blackboard is a Learning Management Hub for Plymouth students">
        <meta name="keywords" content="BG,bG,Bg,bg,Blackboard, LMS, Tutorials, HTML, Learning Management, Learning Hub, ">
        <meta name="author" content="BG Team">
    <!--Favicon-->
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">

    <!--Importing fonts-->
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>

    <!--Importing materialize.css style files-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="all">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--footer css-->
        <link type="text/css" rel="stylesheet" href="css/footer.css">

    <!--CSS IMPORT-->
        <link type="text/css" rel="stylesheet" media="all" href="css/style_main.css">
        <link type="text/css" rel="stylesheet" media="all" href="css/style_sub1.css">

      <style>

          .navbarlogo {
              height: 145px;
              transition: height .45s;
          }



          .navbarlogo.second-state{
              height: 65px;
          }

          #navbarlogo1 {
              height: 140px;
              transition: height .45s;
          }



          #navbarlogo1.second-state{
              height: 65px;
          }

      </style>
  </head>


  <body >

  <div class="brown darken-3" style="position: absolute; width: 100%; height: 130px;">
      <!--This div will cover the slidshow and very top of the body-->
  </div>

    <!--_______________________________________BACK TO TOP BUTTON_______________________________________-->
        <div id="backtotop"></div>
        <a href="#backtotop" class="top hide-on-med-and-down">
            <img style="height: 50px; width: 50px;" src="images/backtotop.png" alt="back to top button">
        </a>
    <!--_______________________________________END OF BACK TO TOP BUTTON_______________________________________-->


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
      
      
      
      
      
      
      
      
      
      
      
      
      
      
    
          
    
    
    
    
    
    
    