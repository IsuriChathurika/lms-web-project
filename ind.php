<!DOCTYPE html>
<html lang="en">
  <head>
        <title>Blackboard LMS</title>

        <meta charset="UTF-8">
        <meta name="description" content="Blackboard is a Learning Management Hub for Plymouth students">
        <meta name="keywords" content="BG,bG,Bg,bg,Blackboard, LMS, Tutorials, HTML, Learning Management, Learning Hub, ">
        <meta name="author" content="BG Team">
    <!--Favicon-->
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">

    <!--Importing fonts-->
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>

    <!--Importing materialize.css style files-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="all">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--footer css-->
        <link type="text/css" rel="stylesheet" href="css/footer.css">

    <!--CSS IMPORT-->
        <link type="text/css" rel="stylesheet" media="all" href="css/style_main.css">
        <link type="text/css" rel="stylesheet" media="all" href="css/style_sub1.css">

      <style>

          .navbarlogo {
              height: 145px;
              transition: height .45s;
          }



          .navbarlogo.second-state{
              height: 65px;
          }

          #navbarlogo1 {
              height: 140px;
              transition: height .45s;
          }



          #navbarlogo1.second-state{
              height: 65px;
          }

      </style>
  </head>


  <body >

  <div class="brown darken-3" style="position: absolute; width: 100%; height: 130px;">
      <!--This div will cover the slidshow and very top of the body-->
  </div>

    <!--_______________________________________BACK TO TOP BUTTON_______________________________________-->
        <div id="backtotop"></div>
        <a href="#backtotop" class="top hide-on-med-and-down">
            <img style="height: 50px; width: 50px;" src="images/backtotop.png" alt="back to top button">
        </a>
    <!--_______________________________________END OF BACK TO TOP BUTTON_______________________________________-->


    <!--_______________________________________NAV BAR_______________________________________-->



    <div class="navbar-fixed">

        <ul id="dropdown1" class="dropdown-content">
            <li><a class="grey-text text-darken-3" href="pages/courses.php">Courses</a></li>
            <li><a  class="grey-text text-darken-3" href="#">Hall Allocations</a></li>
        </ul>

        <ul id="dropdown2" class="dropdown-content">
            <li><a  class="grey-text text-darken-3" href="pages/myacc.php">My Account</a></li>
            <li><a class="grey-text text-darken-3" href="page_register.php">Sign-Up</a></li>
        </ul>

        <ul id="dropdown3" class="dropdown-content">
            <li><a  class="grey-text text-darken-3" href="pages/event.php">Events</a></li>
            <li><a class="grey-text text-darken-3" href="pages/gallery.php">Gallery</a></li>
        </ul>



        <nav class="brown darken-3 navbaropacity navbarlogo"  style="font-family: Raleway;">
            <div class="nav-wrapper">
                <a class="brand-logo center">Blackboard</a>
                <img class="responsive-img  hide-on-med-and-down" id="navbarlogo1" src="images/LogoLMS.png" style="margin-left: 1em;" alt="Logo">
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li style="background-color: brown lighten-5;"><a href="ind.php">Home</a></li>
                    <li><a href="pages/tutorials.php">Tutorials</a></li>
                    <!-- Drop-down Trigger -->
                    <li><a class="dropdown-button" data-activates="dropdown1">Courses<i class="material-icons right"></i></a></li>
                    <li><a class="dropdown-button" data-activates="dropdown3">Events<i class="material-icons right"></i></a></li>
                    <li><a class="dropdown-button" data-activates="dropdown2">Members<i class="material-icons right"></i></a></li>

                </ul>

            </div>

                <ul class="side-nav" id="mobile-demo">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="pages/tutorials.php">Tutorials</a></li>
                    <li class="divider"></li>
                    <li><a href="../pages/courses.php">Courses</a></li>
                    <li><a href="pages/hallallocations.php">Hall Allocations</a></li>
                    <li class="divider"></li>
                    <li><a href="pages/event.php">Events</a></li>
                    <li><a href="pages/gallery.php">Gallary</a></li>
                    <li class="divider"></li>
                    <li><a href="pages/login/redirect.php">My Account</a></li>
                    <li><a href="pages/sign_up.php">Sign-Up</a></li>
                </ul>

        </nav>
    </div>
    <!--_______________________________________END OF NAV BAR_______________________________________-->

    <!--_______________________________________CONTENT_______________________________________-->

    <!--SLIDE SHOW OF THE PAGE-->
    <pre>
    </pre>

        <div class="slider">
            <ul class="slides" style="height: 55em;" >
                <li>
                    <img src="images/slide1.jpg" alt="slide1">
                    <div class="caption center-align">
                        <h3>Blackboard</h3>
                        <h5 class="light grey-text text-lighten-3">The Learning Management System</h5>
                    </div>
                </li>
                <li>
                    <img src="images/slide2.jpg" alt="slide2">
                    <div class="caption left-align">
                        <h3>Learn Online</h3>
                        <h5 class="light grey-text text-lighten-3">Watch Lots of Tutorials Online</h5>
                    </div>
                </li>
                <li>
                    <img src="images/slide3.jpg" alt="slide3">
                    <div class="caption right-align">
                        <h3>All in One</h3>
                        <h5 class="light grey-text text-lighten-3">Interactive, Educational things in One Place</h5>
                    </div>
                </li>
                <li>
                    <img src="images/slide4.jpg" alt="slide4">
                    <div class="caption center-align">
                        <h3>For Plymouth Students</h3>
                        <h5 class="light grey-text text-lighten-3">Our Mission is, to Provide the Best Learning Experience</h5>
                    </div>
                </li>

            </ul>
        </div>
    <!--END OF THE SLIDE SHOW OF THE PAGE-->

    <!--BEGINNING TEXT OF THE PAGE-->
    
    <div class="brown lighten-3 flow-text" style="margin-top: -3em; padding-top: 2em;">
        <div class="container ">
            <h3 style="text-align: center; font-size: 155%; font-weight: 500;">The Blackboard Learning Management System</h3>
            <p style="text-align:center; font-size: 90%;">
                This is the "Blackboard" of Plymouth.<br />
                For Computing students in the University College of Plymouth.<br />
                This site provide Tutorials and Lecture Presentations and other services.<br />
                World-Class Education for students, anywhere.
            </p>
          
                <br />
                
                 <a style="display: block; margin-left: auto; margin-right: auto; width: 15em;" href="pages/login/redirect.php" class="waves-effect waves-light btn-large indigo accent-2 animateblock">
                    <i class="material-icons left" style="font-size: 100%; font-weight: 300;"></i>
                    Login
                </a>
                <br>
        </div>
     </div>
     
     
     <!--____________________Parallax1 ____________________-->
     <div class="parallax-container" style="height: 35em;">
    <div class="parallax"><img src="images/kidwithpc.jpg"></div>
    <br>
    
     <div class="container flow-text" style="height: 20em;">
            <p class="animateblock leftanimtxt light grey-text text-lighten-4" style="font-size: 155%; font-weight: 500;">Now it is easy...</p>
            <p class="animateblock btmanimtxt" style="font-size: 100%;">Suppor for every<br />
                  Devises. Take Your Studies <br />
                  to the Next Level.<br />
                  Get Right to Work and Play.
            </p>
    
  </div>
  
  <div class="section white">
    <div class="row container">
        </div>
        </div>
        </div>
        
<!--_____________________ end of parallax1_______________-->

       <!--____________________Parallax2 ____________________-->
     <div class="parallax-container" style="height: 35em;">
    <div class="parallax"><img src="images/graduat.jpg"></div>
      <div class="container flow-text" style="height: 20em;">
        <div class="row">
            <div class="col m5 offset-m7">
              <p class="animateblock rightanimtxt light grey-text text-lighten-4" style="font-size: 155%; font-weight: 500;">TUTORIALS..!</p>
              <p class="animateblock btmanimtxt" style="font-size: 100%;">Filled With a Rich Content <br />
                  of Tutorials for Your <br />
                  Knowledge. Constantly <br />
                  Uploaded Tutorials by Professional <br />
                  Lectures. Everything at Your <br />
                  Fingertip.
              </p>
            </div>
        </div>
      </div>
  </div>
  <div class="section white">
    <div class="row container">
        </div>
        </div>
        
        
<!--_____________________ end of parallax2_______________-->
    <!--________________________________________CARDS____________________________________________-->

        <div class="brown lighten-3">
            <br />

            <div class="container">
                <div class="row">
           
        <!--_______________CARD ROW 1___________________-->
        <!--________CARD 1_________-->
                   <div class="col s12 l4 m6">
                        <div class="card">
                            <div class="card-image">
                                <img src="images/tale.jpg" alt="card 1" />
                                 
                                <span class="card-title">NSBM We got Talent</span>
                            </div>
                            <div style="height: 15em;" class="card-content">
                                <p>NSBM We got Talent, the massive talent search organised by the Management of National School of Business Management (NSBM) for the third consecutive year...
                                    </p>
                            </div>
                            <div class="card-action">
                                <a href="#">Read more..</a>
                            </div>
                        </div>
                    </div>




        <!--________CARD 2_________-->
                  <div class="col s12 l4 m6">
                        <div class="card">
                            <div class="card-image">
                                <img src="images/grc.jpg" alt="card 2" />
                                <span class="card-title">Plymouth university-Graduation Ceremony</span>
                            </div>
                            <div style="height: 15em;" class="card-content">
                                <p>The inception Graduation Ceremony of the Plymouth University, United Kingdom for the degree programmes offered in collaboration with National School o...</p>
                            </div>
                            <div class="card-action">
                                <a href="#">Read more..</a>
                            </div>
                        </div>
                    </div>




        <!--________CARD 3_________-->


                   <div class="col s12 l4 m6">
                        <div class="card">
                            <div class="card-image">
                                <img src="images/newb.jpeg" alt="card 3" />
                                <span class="card-title">NSBM Green University</span>
                            </div>
                            <div style="height: 15em;" class="card-content">
                                <p>A media briefing and a progress review of the construction of NSBM Green University Town, a venture inspired to take Sri Lankan Higher education to ne...</p>
                            </div>
                            <div class="card-action">
                                <a href="#">Read more..</a>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
       
            

   
    <!--____________________________________END OF CARDS_________________________________________-->





        <div class="flow-text  hide-on-small-only">
                <br />
                <br />
                

                <br />
                <br />
        </div>
                


    <!--_______________________________________END OF CONTENT_______________________________________-->


    <!--_______________________________________FOOTER_______________________________________-->

            <?php include 'header and footer/include_home_footer.php' ?>

    <!--______________________________________________END OF FOOTER______________________________________________-->




    <!--________________________________________________________________________________________________-->

          <!--Import jQuery and materialize.js-->
                <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
                <script type="text/javascript" src="js/materialize.min.js"></script>

         <!--Import jquery.scrollAnimate.js-->
                <script type="text/javascript" src="js/jquery.scrollAnimate.js"></script>
                <script type="text/javascript">

                    $('.dropdown-button').dropdown({
                        inDuration: 300,
                        outDuration: 225,
                        constrain_width: true, // Does not change width of dropdown to that of the activator
                        hover: true, // Activate on hover
                        gutter: 0, // Spacing from edge
                        belowOrigin: false // Displays dropdown below the button
                    });



                    $( document ).ready(function() {
                        $(".button-collapse").sideNav();
                    });



    //SLIDESHOW script below
                    $(document).ready(function(){
                        $('.slider').slider({full_width: true});
                    });



                </script>
    <!--This Content is used to animate items when scrolling. Associated with jquery.scrollAnimate.js-->
    <!--______________________________________________BEGIN OF ANIMATE______________________________________________-->
    <script>
        $(document).ready(function() {
                $('.navbaropacity').scrollAnimate({
                    startScroll: 100,
                    endScroll: 300,
                    cssProperty: 'opacity',
                    before: 1,
                    after:.9
                });


        });
        jQuery(document).ready(function($) {
            $(window).scroll(function() {
                var scroll 		= $(this).scrollTop();


                if (scroll > 150) {
                    $('.navbarlogo').addClass('second-state');


                } else {
                    $('.navbarlogo').removeClass('second-state');


                }
            });

            $(window).scroll(function() {
                var scroll1 		= $(this).scrollTop();


                if (scroll1 > 150) {

                    $('#navbarlogo1').addClass('second-state');

                } else {

                    $('#navbarlogo1').removeClass('second-state');

                }
            });

        });
    </script>

    <!--______________________________________________END OF ANIMATE______________________________________________-->

    <!--______________________________________________BEGIN OF BACK TO TOP JAVASCRIPT______________________________________________-->
    <script>
        $(document).ready(function() {
            var offset=250, // At what pixels show Back to Top Button
            scrollDuration=300; // Duration of scrolling to top
            $(window).scroll(function() {
                if ($(this).scrollTop() > offset)
                    {
                        $('.top').fadeIn(500); // Time(in Milliseconds) of appearing of the Button when scrolling down.
                    }
                else
                    {
                        $('.top').fadeOut(500); // Time(in Milliseconds) of disappearing of Button when scrolling up.
                    }
            });

        // Smooth animation when scrolling
            $('.top').click(function(event) {
                event.preventDefault();
            $('html, body').animate({
                scrollTop: 0}, scrollDuration);
            })
        });
    </script>
    <!--______________________________________________END OF BACK TO TOP JAVASCRIPT______________________________________________-->
    
    <!--______________________Parallax JAVASCRIPT________________-->
    <script>
     $(document).ready(function(){
      $('.parallax').parallax();
    });
    </script>
    
    <!--____________________end of Parallax_____________________-->

    <!--__________________________________BEGIN OF ANIMATING WITH SCROLL(CONTENT ANIMATOR)_______________________________________-->
    <script type="text/javascript">
          $(function(){
              var $elems = $('.animateblock');
              var winheight = $(window).height();
              var fullheight = $(document).height();

              $(window).scroll(function(){
                  animate_elems();
              });

              function animate_elems() {
                  wintop = $(window).scrollTop(); // calculate distance from top of window

                  // loop through each item to check when it animates
                  $elems.each(function(){
                      $elm = $(this);

                      if($elm.hasClass('animated')) { return true; } // if already animated skip to the next item

                      topcoords = $elm.offset().top; // element's distance from top of page in pixels

                      if(wintop > (topcoords - (winheight*.75))) {
                          // animate when top of the window is 3/4 above the element
                          $elm.addClass('animated');
                      }
                  });
              } // end animate_elems()
          });
    </script>
    <!--_____________________________________END OF ANIMATING WITH SCROLL(CONTENT ANIMATOR)______________________________________-->
<script>


</script>


    <!--__________________________________END OF THE PAGE____________________________________________-->
</body>
</html>