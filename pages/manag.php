<!DOCTYPE html>
<html lang="en">
<head>
    <title>Event</title>

    <meta charset="UTF-8">
    <meta name="description" content="Blackboard is a Learning Management Hub for Plymouth Student (University College Plymouth)">
    <meta name="keywords" content="BB,Bb,Blackboard,blackboard, LMS, Tutorials, HTML, Learning Management, Learning Hub, ">
    <meta name="author" content="BlackdoardTeam">
    <!--Favicon-->
    <link rel="icon" href="../images/favicon.ico" type="image/x-icon">

    <!--Importing fonts-->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>

    <!--Importing materialize.css style files-->
    <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="all">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--footer css-->
    <link type="text/css" rel="stylesheet" href="../css/footer.css">

    <!--CSS IMPORT-->
    <link type="text/css" rel="stylesheet" media="all" href="../css/style_main.css">
    <link type="text/css" rel="stylesheet" media="all" href="../css/style_sub1.css">



    <script type="text/javascript">
        function resizeIframe(obj) {
            obj.style.height = obj.contentWindow.document.body.scrollHeight+30 + 'px';
        }
    </script>

    <style>

        .navbarlogo {
            height: 145px;
            transition: height .45s;
        }



        .navbarlogo.second-state{
            height: 65px;
        }

        #navbarlogo1 {
            height: 145px;
            transition: height .49s;
        }



        #navbarlogo1.second-state{
            height: 65px;
        }

    </style>
</head>


<body>
<div class="brown darken-3" style="position: absolute; width: 100%; height: 135px;">
      <!--This div will cover the slidshow and very top of the body-->
  </div>
<!--_______________________________________BACK TO TOP BUTTON_______________________________________-->
<div id="backtotop"></div>
<a href="#backtotop" class="top hide-on-med-and-down">
    <img style="height: 50px; width: 50px;" src="../images/backtotop.png" alt="back to top button">
</a>
<!--_______________________________________END OF BACK TO TOP BUTTON_______________________________________-->


<!--_______________________________________NAV BAR_______________________________________-->



<div class="navbar-fixed">

    <ul id="dropdown1" class="dropdown-content">
        <li><a class="grey-text text-darken-3" href="courses.php">Courses</a></li>
        <li><a  class="grey-text text-darken-3" href="hallallocations.php">Hall Allocations</a></li>
    </ul>

    <ul id="dropdown2" class="dropdown-content">
        <li><a  class="grey-text text-darken-3" href="#">My Account</a></li>
        <li><a class="grey-text text-darken-3" href="signup.php">Sign-Up</a></li>
    </ul>

    <ul id="dropdown3" class="dropdown-content">
        <li><a  class="grey-text text-darken-3" href="events.php">Events</a></li>
        <li><a class="grey-text text-darken-3" href="gallery.php">Gallery</a></li>
    </ul>



    <nav class="brown darken-3 navbaropacity navbarlogo"  style="font-family: Raleway;">
        <div class="nav-wrapper">
            <a class="brand-logo center">Blackboard</a>
            <img class="responsive-img  hide-on-med-and-down" id="navbarlogo1" src="../images/LogoLMS.png" style="margin-left: 1em;" alt="Logo">
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="../ind.php">Home</a></li>
                <li><a href="tutorials.php">Tutorials</a></li>
                <!-- Drop-down Trigger -->
                 <li><a class="dropdown-button" data-activates="dropdown1">Courses<i class="material-icons right"></i></a></li>
                    <li><a class="dropdown-button" data-activates="dropdown3">Events<i class="material-icons right"></i></a></li>
                    <li style="background-color: dimgrey;"><a class="dropdown-button" data-activates="dropdown3">Member<i class="material-icons right"></i></a></li>
            </ul>

        </div>

        <ul class="side-nav" id="mobile-demo">
            <li><a href="../index.php">Home</a></li>
            <li><a href="tutorials.php">Tutorials</a></li>
            <li class="divider"></li>
            <li><a href="../courses.php">Courses</a></li>
            <li><a href="hallallocations.php">Hall Allocations</a></li>
            <li class="divider"></li>
            <li><a href="events.php">Events</a></li>
            <li><a href="gallery.php">Gallary</a></li>
            <li class="divider"></li>
            <li><a href="#">My Account</a></li>
            <li><a href="#">Sign-Up</a></li>
        </ul>

    </nav>
</div>
<!--_______________________________________END OF NAV BAR_______________________________________-->
<!--_______________________________________CONTENT_______________________________________-->

<pre>




</pre>

<ul  style="height: 55em;" >
                <li>
                    <img src="slide1.jpg" width="1500" height="600">
                    <div class="caption center-align">
                      <br>  <h3>Management</h3>
                        <h5 class="col-md-8 col-md-offset-2 desc color-white hidden">Imagine Your Future With No Limits!<br>From exemplary teaching to a campus experience packed with cultural and social opportunities, here NSBM is the very best place to learn and get your future off to a flying start...</h5>
                    </div>
                </li>
</ul>




 
    <h3 style="font-weight:300; text-align: "left-aside"; margin-bottom:2em;;">About Management </h3> 
    <p>The School of Business of NSBM is the ideal institute for any undergraduate interested in pursuing a career in the Business field. The School nurtures students with a business mind and moulds them into fully fledged business leaders of the future. The drive behind achieving this goal is the passion for excellence and perfection that surrounds the School of Business in its methods of teaching, learning, research and networking with the business community.</p>
<p>The School of Business offers many degree programs: Business Management, Human Resource Management, Law and Accounting in collaboration with world renowned universities, University College Dublin, Ireland, Limkokwing University Malaysia and University of Plymouth, UK. This gives our graduates the opportunity to gain an International degree while living and working in Sri Lanka.</p>
    
<p>Students will find learning at NSBM’s School of Business quite a unique and interesting experience as undergraduates are also given the taste of real life business experiences while learning the theories behind it at class. Through this process, the school strives to prepare business undergraduates to face any challenges in the real business world as they will be equipped with excellent problem solving and analytical capabilities. The School of Business takes special care to ensure that all students are provided with intellectual depth and abundant resources as well as individual attention.</p>
        <p>Each department of the School of Business is dedicated to its students and in providing them the best possible educational experience. So, if you’re looking for a bright future in Business, NSBM School of Business is the place to be!</p>

    <h4>Career Development</h4>

<p>Career Development Programmes are an inclusive part of the institutional development. The students are motivated for upgrading their skills and personality on a customary basis. Particularly, the students are treated and trained for analytical skills, human skills, leadership qualities and orientations, communication skills.</p>

 <br>
    <p ><h4>Programmes Offered by School of Business:</h4></p>
    
<ul>
    
    <li>Postgraduate Diploma in Human Resource Management</li>
    <li>Postgraduate Diploma in Business Management</li>
    <li> BSc in Business Management (Human Resource Management) (Special)</li>
    <li>BSc in Business Management (Logistics Management) (Special)</li>
    <li>BSc in Business Management (Project Management) (Special)</li>
    <li>BSc in Business Management (Industrial Management) (Special)</li>
    <li>BSc (Honours) Marketing Management</li>
    <li>BSc (Honours) Accounting and Finance</li>
    <li>BSc (Honours) Operations and Logistics Management</li>
    <li>BSc (Honours) International Management and Business</li>
    <li>BSc (Honours) Events, Tourism and Hospitality Management</li>
    <li>BSc (Honours) Business Communication</li>

</ul>
    
    

    
    
    
    
    
        <!--_______________________________________FOOTER_______________________________________-->


<footer class="page-footer brown darken-3" style="font-family: Raleway;">
    <div class="brown darken-3">

        <div class="row">
            <div class="col m2 s12 offset-s2 offset-m1">
                <img class="responsive-img" src="../images/LogoLMS.png" style="top: 1em; position: relative;" alt="Logo">
            </div>
            <div class="col m5 s12">
                <h5 class="white-text">Blackboard Learning Management System</h5>
                <p class="grey-text text-lighten-4">
                    Blackboard is optimized for learning. Tutorials, references, and
                    examples are constantly reviewed to avoid errors, but we cannot
                    warrant full correctness of all content. While using this site,
                    you agree to our terms of use.
                </p>
                <br />
                <br />
                <a href="../index.php"><img class="icon" src="../images/LogoLMS.png" style="height: 50px; width: 50px;" alt="Logo"></a>
               
            </div>
            <div class="col m2 s12 offset-m1">
                <h6 class="white-text">Visit Us</h6>
                <ul>
                    <li><a class="grey-text text-lighten-3 icon" target="_blank" href="#">
                        <img style="height: 57px; width: 50px;" src="../images/fb.png" alt="fb"></a>
                    </li>
                    <li><a class="grey-text text-lighten-3 icon" target="_blank" href="#">
                        <img style="height: 57px; width: 50px;" src="../images/twitter.png" alt="twitter"></a>
                    </li>
                    <li><a class="grey-text text-lighten-3 icon" target="_blank" href="#">
                        <img style="height: 57px; width: 50px;" src="../images/gplus.png" alt="GPlus"></a>
                    </li>
                </ul>
            </div>
        </div>





       
        <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #424242; padding: 0; margin-left: 15px; margin-right: 15px;"/>

        <div class="footer-copyright brown darken-1 brown lighten-5-text text-darken-2 ">
            <div class="container">
                Copyright © Blackboard 2015 . All Rights Reserved.
            </div>

        </div>

    </div>
</footer>


<!--______________________________________________END OF FOOTER______________________________________________-->




<!--________________________________________________________________________________________________-->

<!--Import jQuery and materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="../js/materialize.min.js"></script>

<!--Import jquery.scrollAnimate.js-->
<script type="text/javascript" src="../js/jquery.scrollAnimate.js"></script>
<script type="text/javascript">

    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrain_width: true, // Does not change width of dropdown to that of the activator
        hover: true, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: false // Displays dropdown below the button
    });



    $( document ).ready(function() {
        $(".button-collapse").sideNav();
    });



    //SLIDESHOW script below
    $(document).ready(function(){
        $('.slider').slider({full_width: true});
    });



</script>
<!--This Content is used to animate items when scrolling. Associated with jquery.scrollAnimate.js-->
<!--______________________________________________BEGIN OF ANIMATE______________________________________________-->
<script>
    $(document).ready(function() {
        $('.navbaropacity').scrollAnimate({
            startScroll: 100,
            endScroll: 300,
            cssProperty: 'opacity',
            before: 1,
            after:.9
        });


    });
    jQuery(document).ready(function($) {
        $(window).scroll(function() {
            var scroll 		= $(this).scrollTop();


            if (scroll > 50) {
                $('.navbarlogo').addClass('second-state');


            } else {
                $('.navbarlogo').removeClass('second-state');


            }
        });

        $(window).scroll(function() {
            var scroll1 		= $(this).scrollTop();


            if (scroll1 > 50) {

                $('#navbarlogo1').addClass('second-state');

            } else {

                $('#navbarlogo1').removeClass('second-state');

            }
        });

    });
</script>

<!--______________________________________________END OF ANIMATE______________________________________________-->

<!--______________________________________________BEGIN OF BACK TO TOP JAVASCRIPT______________________________________________-->
<script>
    $(document).ready(function() {
        var offset=250, // At what pixels show Back to Top Button
                scrollDuration=300; // Duration of scrolling to top
        $(window).scroll(function() {
            if ($(this).scrollTop() > offset)
            {
                $('.top').fadeIn(500); // Time(in Milliseconds) of appearing of the Button when scrolling down.
            }
            else
            {
                $('.top').fadeOut(500); // Time(in Milliseconds) of disappearing of Button when scrolling up.
            }
        });

        // Smooth animation when scrolling
        $('.top').click(function(event) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: 0}, scrollDuration);
        })
    });
</script>
<!--______________________________________________END OF BACK TO TOP JAVASCRIPT______________________________________________-->



<!--__________________________________END OF THE PAGE____________________________________________-->
</body>
</html>
        
       