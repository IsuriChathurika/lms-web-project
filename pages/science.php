<!DOCTYPE html>
<html lang="en">
<head>
    <title>Event</title>

    <meta charset="UTF-8">
    <meta name="description" content="Blackboard is a Learning Management Hub for Plymouth Student (University College Plymouth)">
    <meta name="keywords" content="BB,Bb,Blackboard,blackboard, LMS, Tutorials, HTML, Learning Management, Learning Hub, ">
    <meta name="author" content="BlackdoardTeam">
    <!--Favicon-->
    <link rel="icon" href="../images/favicon.ico" type="image/x-icon">

    <!--Importing fonts-->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>

    <!--Importing materialize.css style files-->
    <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="all">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--footer css-->
    <link type="text/css" rel="stylesheet" href="../css/footer.css">

    <!--CSS IMPORT-->
    <link type="text/css" rel="stylesheet" media="all" href="../css/style_main.css">
    <link type="text/css" rel="stylesheet" media="all" href="../css/style_sub1.css">



    <script type="text/javascript">
        function resizeIframe(obj) {
            obj.style.height = obj.contentWindow.document.body.scrollHeight+30 + 'px';
        }
    </script>

    <style>

        .navbarlogo {
            height: 145px;
            transition: height .45s;
        }



        .navbarlogo.second-state{
            height: 65px;
        }

        #navbarlogo1 {
            height: 145px;
            transition: height .49s;
        }



        #navbarlogo1.second-state{
            height: 65px;
        }

    </style>
</head>


<body>
<div class="brown darken-3" style="position: absolute; width: 100%; height: 135px;">
      <!--This div will cover the slidshow and very top of the body-->
  </div>
<!--_______________________________________BACK TO TOP BUTTON_______________________________________-->
<div id="backtotop"></div>
<a href="#backtotop" class="top hide-on-med-and-down">
    <img style="height: 50px; width: 50px;" src="../images/backtotop.png" alt="back to top button">
</a>
<!--_______________________________________END OF BACK TO TOP BUTTON_______________________________________-->


<!--_______________________________________NAV BAR_______________________________________-->



<div class="navbar-fixed">

    <ul id="dropdown1" class="dropdown-content">
        <li><a class="grey-text text-darken-3" href="courses.php">Courses</a></li>
        <li><a  class="grey-text text-darken-3" href="hallallocations.php">Hall Allocations</a></li>
    </ul>

    <ul id="dropdown2" class="dropdown-content">
        <li><a  class="grey-text text-darken-3" href="#">My Account</a></li>
        <li><a class="grey-text text-darken-3" href="signup.php">Sign-Up</a></li>
    </ul>

    <ul id="dropdown3" class="dropdown-content">
        <li><a  class="grey-text text-darken-3" href="events.php">Events</a></li>
        <li><a class="grey-text text-darken-3" href="gallery.php">Gallery</a></li>
    </ul>



    <nav class="brown darken-3 navbaropacity navbarlogo"  style="font-family: Raleway;">
        <div class="nav-wrapper">
            <a class="brand-logo center">Blackboard</a>
            <img class="responsive-img  hide-on-med-and-down" id="navbarlogo1" src="../images/LogoLMS.png" style="margin-left: 1em;" alt="Logo">
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="../ind.php">Home</a></li>
                <li><a href="tutorials.php">Tutorials</a></li>
                <!-- Drop-down Trigger -->
                 <li><a class="dropdown-button" data-activates="dropdown1">Courses<i class="material-icons right"></i></a></li>
                    <li><a class="dropdown-button" data-activates="dropdown3">Events<i class="material-icons right"></i></a></li>
                    <li style="background-color: dimgrey;"><a class="dropdown-button" data-activates="dropdown3">Member<i class="material-icons right"></i></a></li>
            </ul>

        </div>

        <ul class="side-nav" id="mobile-demo">
            <li><a href="../index.php">Home</a></li>
            <li><a href="tutorials.php">Tutorials</a></li>
            <li class="divider"></li>
            <li><a href="../courses.php">Courses</a></li>
            <li><a href="hallallocations.php">Hall Allocations</a></li>
            <li class="divider"></li>
            <li><a href="events.php">Events</a></li>
            <li><a href="gallery.php">Gallary</a></li>
            <li class="divider"></li>
            <li><a href="#">My Account</a></li>
            <li><a href="#">Sign-Up</a></li>
        </ul>

    </nav>
</div>
<!--_______________________________________END OF NAV BAR_______________________________________-->
<!--_______________________________________CONTENT_______________________________________-->

<pre>




</pre>
        <ul  style="height: 55em;" >
                <li>
                    <img src="science.jpg" width="1500" height="600">
                    <div class="caption center-align">s
                      <br>  <h3>science</h3>
                        <h5 class="col-md-8 col-md-offset-2 desc color-white hidden">Imagine Your Future With No Limits!<br>From exemplary teaching to a campus experience packed with cultural and social opportunities, here NSBM is the very best place to learn and get your future off to a flying start...</h5>
                    </div>
                </li>
</ul>
    


        <h3 style="font-weight:300; text-align:left_aside; margin-bottom:2em;;">About Faculty of Science</h3>
    <p>B.Sc. Degree programme is designed to contribute towards raising the general educational standards of the public by providing an opportunity for higher education in Science. This programme facilitates those already employed in the field and late developers to acquire a B.Sc. degree in their own time at an affordable cost. It specifically aims to:</p>

       <ol>
           <li> provide an opportunity for persons to obtain a Science Degree and thereby improve their promotional/career prospects</li>
           <li> enable, particularly those already employed in the field, late developers, and those who could not avail themselves of higher                    education at the end of their secondary school career to obtain a degree</li>
           <li> enable any person to pursue a degree course in Science in their own time at an affordable cost</li>
           <li> combine courses within and outside the Faculty to suit a student’s interest or job requirements</li>

       </ol>

     <p>The mode of delivery of the programme is based on Open and Distance Learning (ODL) methods. It is different to the conventional study system most students are familiar with. Student learning is facilitated through carefully prepared printed course material suitable for self learning. Printed course material is supplemented with audio visual and online learning material.</p>
    
     <p>Faculty of Natural Sciences offers courses at different Levels. Degree level courses are at Level 3, 4, 5 & 6. The Level 3-5 is comparable with the General Degree programme of the conventional university system, while Level 3- 6 represents a special Degree programme. The medium of instruction is English. However, Level 3 courses can be offered in Sinhala, English or the Tamil Medium. The Faculty of Natural Sciences has six Departments of study: Botany, Chemistry, Mathematics & Computer Science, Physics, Zoology and Health Sciences, which offer range of discipline-based courses.</p>
    
    <p>The B.Sc. programme offers courses at Levels 3, 4 and 5/6, equivalent to the 1st, 2nd, and 3rd years of the B.Sc. Degree programme of a conventional university. Depending on the qualifications, there are three disciplines to be chosen from among Botany, Chemistry, Pure Maths, Applied Maths, Computer Science, Physics or Zoology for the B.Sc. Degree.</p>
    
     
    <h4>Departments of N.Sc</h4>
    
    
  <ul>
    
      <li> Botany </li>
      <li>Chemistry</li>
      <li>Health Science</li>
      <li>Mathematics & Computer Science</li>
      <li>Physics</li>
      <li>Zoology</li>

    
  </ul>
    
    
    
    
    
    
<!--________________________________________________________________________________________________-->

<!--Import jQuery and materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="../js/materialize.min.js"></script>

<!--Import jquery.scrollAnimate.js-->
<script type="text/javascript" src="../js/jquery.scrollAnimate.js"></script>
<script type="text/javascript">

    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrain_width: true, // Does not change width of dropdown to that of the activator
        hover: true, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: false // Displays dropdown below the button
    });



    $( document ).ready(function() {
        $(".button-collapse").sideNav();
    });



    //SLIDESHOW script below
    $(document).ready(function(){
        $('.slider').slider({full_width: true});
    });



</script>
<!--This Content is used to animate items when scrolling. Associated with jquery.scrollAnimate.js-->
    
   
    <!--_______________________________________FOOTER_______________________________________-->


<footer class="page-footer brown darken-3" style="font-family: Raleway;">
    <div class="brown darken-3">

        <div class="row">
            <div class="col m2 s12 offset-s2 offset-m1">
                <img class="responsive-img" src="../images/LogoLMS.png" style="top: 1em; position: relative;" alt="Logo">
            </div>
            <div class="col m5 s12">
                <h5 class="white-text">Blackboard Learning Management System</h5>
                <p class="grey-text text-lighten-4">
                    Blackboard is optimized for learning. Tutorials, references, and
                    examples are constantly reviewed to avoid errors, but we cannot
                    warrant full correctness of all content. While using this site,
                    you agree to our terms of use.
                </p>
                <br />
                <br />
                <a href="../index.html"><img class="icon" src="../images/LogoLMS.png" style="height: 50px; width: 50px;" alt="Logo"></a>
               
            </div>
            <div class="col m2 s12 offset-m1">
                <h6 class="white-text">Visit Us</h6>
                <ul>
                    <li><a class="grey-text text-lighten-3 icon" target="_blank" href="#">
                        <img style="height: 57px; width: 50px;" src="../images/fb.png" alt="fb"></a>
                    </li>
                    <li><a class="grey-text text-lighten-3 icon" target="_blank" href="#">
                        <img style="height: 57px; width: 50px;" src="../images/twitter.png" alt="twitter"></a>
                    </li>
                    <li><a class="grey-text text-lighten-3 icon" target="_blank" href="#">
                        <img style="height: 57px; width: 50px;" src="../images/gplus.png" alt="GPlus"></a>
                    </li>
                </ul>
            </div>
        </div>





       
        <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #424242; padding: 0; margin-left: 15px; margin-right: 15px;"/>

        <div class="footer-copyright brown darken-1 brown lighten-5-text text-darken-2 ">
            <div class="container">
                Copyright © Blackboard 2015 . All Rights Reserved.
            </div>

        </div>

    </div>
</footer>


<!--______________________________________________END OF FOOTER______________________________________________-->
 
    
    
<!--______________________________________________BEGIN OF ANIMATE______________________________________________-->
<script>
    $(document).ready(function() {
        $('.navbaropacity').scrollAnimate({
            startScroll: 100,
            endScroll: 300,
            cssProperty: 'opacity',
            before: 1,
            after:.9
        });


    });
    jQuery(document).ready(function($) {
        $(window).scroll(function() {
            var scroll 		= $(this).scrollTop();


            if (scroll > 50) {
                $('.navbarlogo').addClass('second-state');


            } else {
                $('.navbarlogo').removeClass('second-state');


            }
        });

        $(window).scroll(function() {
            var scroll1 		= $(this).scrollTop();


            if (scroll1 > 50) {

                $('#navbarlogo1').addClass('second-state');

            } else {

                $('#navbarlogo1').removeClass('second-state');

            }
        });

    });
</script>

<!--______________________________________________END OF ANIMATE______________________________________________-->

<!--______________________________________________BEGIN OF BACK TO TOP JAVASCRIPT______________________________________________-->
<script>
    $(document).ready(function() {
        var offset=250, // At what pixels show Back to Top Button
                scrollDuration=300; // Duration of scrolling to top
        $(window).scroll(function() {
            if ($(this).scrollTop() > offset)
            {
                $('.top').fadeIn(500); // Time(in Milliseconds) of appearing of the Button when scrolling down.
            }
            else
            {
                $('.top').fadeOut(500); // Time(in Milliseconds) of disappearing of Button when scrolling up.
            }
        });

        // Smooth animation when scrolling
        $('.top').click(function(event) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: 0}, scrollDuration);
        })
    });
</script>
<!--______________________________________________END OF BACK TO TOP JAVASCRIPT______________________________________________-->



<!--__________________________________END OF THE PAGE____________________________________________-->
</body>
</html>
        
       