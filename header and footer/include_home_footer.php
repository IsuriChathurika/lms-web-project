
    <!--______________________________________________ FOOTER______________________________________________-->
 
    <footer class="page-footer brown darken-3" style="font-family: Raleway;"  >
            <div class="brown darken-3">

                     <div class="row">
                        <div class="col m2 s12 offset-s2 offset-m1">
                            <img class="responsive-img" src="images/LogoLMS.png" style="top: 1em; position: relative;" alt="Logo">
                        </div>
                        <div class="col m6 s12">
                            <h4 class="white-text">Blackboard Learning Management System</h4>
                            <p class="grey-text text-lighten-4">
                                Blackboard is optimized for learning. Tutorials, references, and
                                examples are constantly reviewed to avoid errors, but we cannot
                                warrant full correctness of all content. While using this site,
                                you agree to our terms of use.
                            </p>
                            <br />
                            <br />
                            <a href="ind.html"><img class="icon" src="images/LogoLMS.png" style="height: 50px; width: 50px;" alt="Logo"></a>
                             <a href="https://www.plymouth.ac.uk/"><img class="icon" src="images/20-plymouth.png" style="height: 50px; width: 50px;" alt="Logo"></a>
                            
                        </div>
                        <div class="col m2 s12 offset-m1">
                            <h5 class="white-text">Visit Us</h5>
                            <ul>
                                <li><a class="grey-text text-lighten-3 icon" target="_blank" href="https://www.facebook.com/plymouthuni">
                                    <img style="height: 57px; width: 50px;" src="images/fb.png" alt="fb"></a>
                                </li>
                                <li><a class="grey-text text-lighten-3 icon" target="_blank" href="https://twitter.com/PlymUni">
                                    <img style="height: 57px; width: 50px;" src="images/twitter.png" alt="twitter"></a>
                                </li>
                                <li><a class="grey-text text-lighten-3 icon" target="_blank" href="https://plus.google.com/+plymouthuniversity">
                                    <img style="height: 57px; width: 50px;" src="images/gplus.png" alt="GPlus"></a>
                                </li>
                            </ul>
                        </div>
                    </div>





       

                <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #424242; padding: 0; margin-left: 15px; margin-right: 15px;"/>

                <div class="footer-copyright brown darken-1 brown lighten-5-text text-darken-2 ">
                    <div class="container">
                        Copyright © Blackboard 2015 . All Rights Reserved.
                    </div>

                </div>

            </div>
        </footer>

 <!--______________________________________________END OF FOOTER______________________________________________-->
