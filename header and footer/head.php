 <!--_______________________________________NAV BAR_______________________________________-->



    <div class="navbar-fixed">

        <ul id="dropdown1" class="dropdown-content">
            <li><a class="grey-text text-darken-3" href="pages/courses.php">Courses</a></li>
            <li><a  class="grey-text text-darken-3" href="#">Hall Allocations</a></li>
        </ul>

        <ul id="dropdown2" class="dropdown-content">
            <li><a  class="grey-text text-darken-3" href="pages/myacc.php">My Account</a></li>
            <li><a class="grey-text text-darken-3" href="#">Sign-Up</a></li>
        </ul>

        <ul id="dropdown3" class="dropdown-content">
            <li><a  class="grey-text text-darken-3" href="pages/event.php">Events</a></li>
            <li><a class="grey-text text-darken-3" href="pages/gallery.php">Gallery</a></li>
        </ul>



        <nav class="brown darken-3 navbaropacity navbarlogo"  style="font-family: Raleway;">
            <div class="nav-wrapper">
                <a class="brand-logo center">Blackboard</a>
                <img class="responsive-img  hide-on-med-and-down" id="navbarlogo1" src="images/LogoLMS.png" style="margin-left: 1em;" alt="Logo">
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li style="background-color: brown lighten-5;"><a href="ind.php">Home</a></li>
                    <li><a href="pages/tutorials.php">Tutorials</a></li>
                    <!-- Drop-down Trigger -->
                    <li><a class="dropdown-button" data-activates="dropdown1">Courses<i class="material-icons right"></i></a></li>
                    <li><a class="dropdown-button" data-activates="dropdown3">Events<i class="material-icons right"></i></a></li>
                    <li><a class="dropdown-button" data-activates="dropdown2">Members<i class="material-icons right"></i></a></li>

                </ul>

            </div>

                <ul class="side-nav" id="mobile-demo">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="pages/tutorials.php">Tutorials</a></li>
                    <li class="divider"></li>
                    <li><a href="../pages/courses.php">Courses</a></li>
                    <li><a href="pages/hallallocations.php">Hall Allocations</a></li>
                    <li class="divider"></li>
                    <li><a href="pages/event.php">Events</a></li>
                    <li><a href="pages/gallery.php">Gallary</a></li>
                    <li class="divider"></li>
                    <li><a href="pages/login/redirect.php">My Account</a></li>
                    <li><a href="pages/signup.php">Sign-Up</a></li>
                </ul>

        </nav>
    </div>
    
    